package com.lffq.myapplication.network;

import com.lffq.myapplication.network.model.News;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {

    @GET("/v2/everything")
    public Call<News> get(@Query("q") String query,
                          @Query("language") String lang,
                          @Query("apiKey") String key);
}
