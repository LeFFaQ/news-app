package com.lffq.myapplication.ui.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.lffq.myapplication.network.NetworkService;
import com.lffq.myapplication.network.model.News;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {

    public HomeViewModel() {

    }

    // Create a LiveData with a String
    final MutableLiveData<News> currentNewsLiveData = new MutableLiveData<>();

    public MutableLiveData<News> getCurrentNews() {

        NetworkService.getInstance()
                .getApi()
                .get("Moscow", "ru", "f0d0603fab884ba0a32a4d0682eb1a92")
                .enqueue(new Callback<News>() {
                    @Override
                    public void onResponse(@NonNull Call<News> call, @NonNull Response<News> response) {

                        currentNewsLiveData.postValue(response.body());

                    }

                    @Override
                    public void onFailure(@NonNull Call<News> call, @NonNull Throwable t) {

                        t.printStackTrace();
                    }
                });

        return currentNewsLiveData;

    }
}