package com.lffq.myapplication.ui.home;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.lffq.myapplication.R;
import com.lffq.myapplication.network.model.News;

import static android.content.ContentValues.TAG;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    RecyclerView recyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        //рекукла

        initRecyclerView(root);

        homeViewModel.getCurrentNews().observe(getViewLifecycleOwner(), new Observer<News>() {
            @Override
            public void onChanged(@Nullable News news) {
                Log.d(TAG, "onChanged: True");
            }

        });
        return root;
    }


    //инициализация Рекуклы
    private void initRecyclerView(View root) {
        recyclerView = root.findViewById(R.id.recyclerList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


    }
}