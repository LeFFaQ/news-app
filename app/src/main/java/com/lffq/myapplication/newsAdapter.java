package com.lffq.myapplication;

import android.icu.text.CaseMap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class newsAdapter extends RecyclerView.Adapter<newsAdapter.newsViewHolder> {

    class newsViewHolder extends RecyclerView.ViewHolder {

        private ImageView newsImage;
        private TextView newsTitle;
        private TextView newsContent;

        public newsViewHolder(View itemView) {
            super(itemView);
        }
    }
}
